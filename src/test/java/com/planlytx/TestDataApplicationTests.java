package com.planlytx;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.planlytx.entities.Entities;
import com.planlytx.repo.Repositories;
import com.planlytx.service.Services;

@SpringBootTest
class TestDataApplicationTests {

	@InjectMocks
	private Services service;

	@Mock
	private Repositories repository;

	@Mock
	private Entities entities;

	@Test
	void contextLoads() {
	}

	@Test
	public void testFindAll() {
		List<Entities> list = new ArrayList<Entities>();
		list.add(new Entities(111, "Nitesh", 20000));
		list.add(new Entities(112, "Shubham", 20000));
		list.add(new Entities(113, "Shivam", 20000));
		when(repository.findAll()).thenReturn(list);
		List<Entities> result = service.getAllData();
		verify(repository, atLeastOnce()).findAll();
		assertNotNull(result);
		assertFalse(result.isEmpty());
	}

}
