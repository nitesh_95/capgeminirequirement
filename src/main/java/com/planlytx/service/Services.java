package com.planlytx.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.planlytx.entities.Entities;
import com.planlytx.repo.Repositories;

@Service
public class Services {

	@Autowired
	private Repositories repositories;

	public List<Entities> getAllData() {
		List<Entities> employeeListData = repositories.findAll();
		if (employeeListData.size() > 0) {
			return employeeListData;
		} else {
			return new ArrayList<Entities>();
		}
	}
}
