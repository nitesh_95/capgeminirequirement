package com.planlytx.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.planlytx.entities.Entities;

@Repository
public interface Repositories extends JpaRepository<Entities, Integer> {
	
}
