package com.planlytx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestDataApplication {

	public static void main(String[] args) {
		Logger logger = LoggerFactory.getLogger(TestDataApplication.class);
        logger.info("This is how you configure Java Logging with SLF4J");
		SpringApplication.run(TestDataApplication.class, args);
	}

}
