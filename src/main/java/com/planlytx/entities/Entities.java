package com.planlytx.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class Entities {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int employeeid;
	private String employeename;
	private int employeesalary;
	
	

	public Entities() {
		super();
	}

	public Entities(int employeeid, String employeename, int i) {
		super();
		this.employeeid = employeeid;
		this.employeename = employeename;
		this.employeesalary = i;
	}

	public int getEmployeeid() {
		return employeeid;
	}

	public void setEmployeeid(int employeeid) {
		this.employeeid = employeeid;
	}

	public String getEmployeename() {
		return employeename;
	}

	public void setEmployeename(String employeename) {
		this.employeename = employeename;
	}

	public int getEmployeesalary() {
		return employeesalary;
	}

	public void setEmployeesalary(int employeesalary) {
		this.employeesalary = employeesalary;
	}

}
