package com.planlytx.controller;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.planlytx.entities.Entities;
import com.planlytx.service.Services;

@RestController
public class Controllers {
	private static final Logger LOG = LoggerFactory.getLogger(Controllers.class);
	@Autowired
	private Services services;

	@GetMapping("/getalldata")
	public List<Entities> getalldata() {
		LOG.debug("debug message");
		LOG.info("This is an info message");
		LOG.warn("Warning for this application");
		LOG.error("Seems some error in the application");
		List<Entities> allData = services.getAllData();
		return allData;
	}
}
